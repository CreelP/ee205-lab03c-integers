###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 03c - integers - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build and test a program that explores integers
###
### @author  @Creel Patrocinio <creel@hawaii.edu>
### @date    2/3/2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

# Build a datatype explorer program

HOSTNAME=$(shell hostname --short)
$(info $(HOSTNAME))

CC     = gcc
CFLAGS = -g -Wall -DHOST=\"$(HOSTNAME)\"

TARGET = integers

all: clearscreen $(TARGET)

.PHONY: clearscreen

clearscreen:
	clear

# For now, do not use Make's % expansion parameters.  Each file must have its own 
# build instructions.  We will use the % expansion later.

integers.o: integers.c integers.h
	$(CC) $(CFLAGS) -c integers.c

integers: integers.o
	$(CC) $(CFLAGS) -o $(TARGET) integers.o

test: clearscreen $(TARGET)
	./$(TARGET)

clean:
	rm -f *.o $(TARGET)
