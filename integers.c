///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03c - Integers - EE 205 - Spr 2022
///
/// Usage:  integers
///
/// Result:
///   Print out platform datatype information from <limits.h> 
///
/// Example:
///   $ ./integers
///  @todo See the .PDF file for the program's output
///
/// @file integers.c
/// @version 1.0
///
/// @warning This program must be compiled on the platform it's exploring.
///
/// @see https://en.wikipedia.org/wiki/Integer_overflow
///
/// @author Creel Patrocinio <creel@hawaii.edu>
/// @date   2/3/2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "integers.h"


/// Print the overflow/underflow characteristics of the "char" datatype
void flowChar() {
   char overflow = CHAR_MAX;
   printf("char overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   char underflow = CHAR_MIN;
   printf("char underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}


/// Print the overflow/underflow characteristics of the "signed char" datatype
void flowSignedChar() {
   signed char overflow = SCHAR_MAX;
   printf("signed char overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   signed char underflow = SCHAR_MIN;
   printf("signed char underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}


/// Print the overflow/underflow characteristics of the "unsigned char" datatype
void flowUnsignedChar() {
   unsigned char overflow = UCHAR_MAX;
   printf("unsigned char overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   unsigned char underflow = 0;  // Note:  See limits.h UCHAR_MAX... there's no UCHAR_MIN, it's 0.
   printf("unsigned char underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}



/// Print the overflow/underflow characteristics of the "short" datatype
void flowShort() {
	/// @todo This function has been stubbed out
   short overflow = SHRT_MAX;
   printf("short overflow: %d +1", overflow++);
   printf("becomes %d\n", overflow);
   
   short underflow = SHRT_MIN;
   printf("short underflow %d -1 ", underflow--);
   printf("becomes %d\n", underflow);


}

/// Print the overflow/underflow characteristics of the "unsigned short" datatype
void flowUnsignedShort() {
	/// @todo This function has been stubbed out
   unsigned short overflow = SHRT_MAX;
   printf("unsigned short overflow: %d +1", overflow++);
   printf("becomes %d\n", overflow);

   unsigned short underflow = 0;
   printf("unsigned short underflow %d -1 ", underflow--);
   printf("becomes %d\n", underflow);

}


void flowInt(){
   int overflow = INT_MAX;
   printf("int overflow %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   int underflow = 0;
   printf("int underflow %d - 1 ", underflow--);
   printf("becomes %d\n",  underflow);


}


void flowUnsignedInt(){
   unsigned int overflow = UINT_MAX;
   printf("unsigned int %u + 1 ", overflow++);
   printf("becomes %u\n", overflow);

   unsigned int underflow = 0;
   printf("unsigned int %u - 1 ", underflow--);
   printf("becomes %un\n", underflow);

}  

void flowLong(){
   long overflow = LONG_MAX;
   printf("long overflow %ld + 1 ", overflow++);
   printf("becomes %ld\n", overflow);

   long underflow = 0;
   printf("long underflow %ld -1", underflow--);
   printf("becomes %ld\n", overflow);

}



void flowUnsignedLong(){

   unsigned long overflow = ULONG_MAX;
   printf("unsigned long overflow %lu + 1 ", overflow++);
   printf("becomes %lu\n", overflow);

   unsigned long underflow = 0;
   printf("unsinged long underflow %lu - 1", underflow--);
   printf("becomes %lu\n ", underflow);


}



///////////////////////////////////////////////////////////////////////////////
/// main

int main( int argc, char* argv[] ) {
	printf("Platform C Datatype explorer\n");
   printf("\n");
   printf("This program must be compiled on the platform it's exploring.  Can you guess why?\n");
   printf("\n");
   printf("This program [%s] was compiled on [%s] at [%s] [%s]\n", argv[0], HOST, __DATE__, __TIME__);

#ifdef __INTEL_COMPILER
   printf("The Intel C++ compiler was used for compiling this program\n");
#elif __GNUC__
   printf("The GNU C Compiler version [%s] was used for compiling this program\n", __VERSION__);
#elif _MSC_VER
   printf("The compiler is Microsoft Visual Studio version [%d]\n", _MSC_VER);
#else
   printf("Can not determine the compiler\n");
#endif

   printf ("\n");
   printf (TABLE_HEADER1);
   printf (TABLE_HEADER2);

   printf(TABLE_FORMAT_CHAR, "char", CHAR_BIT, CHAR_BIT/8, CHAR_MIN, CHAR_MAX);
   printf(TABLE_FORMAT_CHAR, "signed char", CHAR_BIT, CHAR_BIT/8, SCHAR_MIN, SCHAR_MAX);
   printf(TABLE_FORMAT_CHAR, "unsigned char", CHAR_BIT, CHAR_BIT/8, 0, UCHAR_MAX);  // See the comment next to UCHAR_MAX in limits.h
   printf(TABLE_FORMAT_SHORT, "short", sizeof(short)*8, sizeof(short), SHRT_MIN, SHRT_MAX);
	// @todo do the unsigned short... and then keep going
   
   printf(TABLE_FORMAT_SHORT, "unsigned short", sizeof(short)*8, sizeof(short), 0, USHRT_MAX); 
   printf(TABLE_FORMAT_INT, "int", sizeof(int)*8, sizeof(int), INT_MIN, INT_MAX);
   printf(TABLE_FORMAT_UINT, "unsigned int", sizeof(int)*8, sizeof(int), 0, UINT_MAX);
   printf(TABLE_FORMAT_LONG, "long", sizeof(long)*8, sizeof(long), LONG_MIN, LONG_MAX);
   printf(TABLE_FORMAT_ULONG, "unsigned long", sizeof(long)*8, sizeof(long), 0, ULONG_MAX);





   printf ("\n");

   flowChar();
   flowSignedChar();
   flowUnsignedChar();
   flowShort();         /// @todo:  Currently stubbed out
   flowUnsignedShort(); /// @todo:  Currently stubbed out
	// @todo Now, keep writing functions for int, unsigned int, long and unsigned long
   flowInt();
   flowUnsignedInt();
   flowLong();
   flowUnsignedLong();






   return 0;
}
